vim.opt.wrap = true -- wrap lines

local formatters = require "lvim.lsp.null-ls.formatters"

local linters = require "lvim.lsp.null-ls.linters"

lvim.plugins = {
  "stevearc/dressing.nvim",
  {
    "npxbr/glow.nvim",
    ft = {"markdown"},
    config = true, cmd = "Glow"
  },
  {
  "folke/trouble.nvim",
  dependencies = { "nvim-tree/nvim-web-devicons" },
  }
}
